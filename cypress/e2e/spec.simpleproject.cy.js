describe("Airs Ticket", () => {
  // data
  const ctaMessage = "Seats available! Call 0800 MARSAIR to book!";
  const defaultDepart = "July";

  beforeEach(() => {
    cy.visit("/OanhTran");
  });

  it.only("01. There are seats found via Search", () => {
    const returning = "December (two years from now)";

    // Perform Search
    cy.get("#departing").select(defaultDepart);
    cy.get("#returning").select(returning);
    cy.get('[type="submit"]').click();

    // Assertion Search Results page
    cy.get("#content>p:nth-of-type(1)").should("have.text", "Seats available!");

    cy.get("#content>p:last-child>a")
      .should("contain.text", "Back")
      .and("have.attr", "href");

    cy.get("#content>p:nth-of-type(2)").should("have.text", ctaMessage);
  });

  it("02. There are no seats found via Search", () => {
    const returning = "July (two years from now)";

    // Perform Search
    cy.get("#departing").select(defaultDepart);
    cy.get("#returning").select(returning);
    cy.get('[type="submit"]').click();

    // Assertion Search Results page
    cy.get("#content>p:nth-of-type(1)").should(
      "have.text",
      "Sorry, there are no more seats available."
    );

    cy.get("#content>p:last-child>a")
      .should("contain.text", "Back")
      .and("have.attr", "href");
  });

  it("03. There are seats found when returning less than 2 years from Departing", () => {
    const returning = "December";

    // Perform Search
    cy.get("#departing").select(defaultDepart);
    cy.get("#returning").select(returning);
    cy.get('[type="submit"]').click();

    // Assertion Search Results page
    cy.get("#content>p:nth-of-type(1)").should(
      "have.text",
      "Unfortunately, this schedule is not possible. Please try again."
    );

    cy.get("#content>p:last-child>a")
      .should("contain.text", "Back")
      .and("have.attr", "href");
  });

  it("04. There are seats found via Search with valid Promo Code", () => {
    const returning = "December (two years from now)";
    const promoCode = "AF3-FJK-418";
    const promoCodeMessage = `Promotional code ${promoCode} used: ${promoCode.charAt(
      2
    )}0% discount!`;

    // Perform Search
    cy.get("#departing").select(defaultDepart);
    cy.get("#returning").select(returning);
    cy.get("#promotional_code").type(promoCode);
    cy.get('[type="submit"]').click();

    // Assertion Search Results page
    cy.get("#content>p:nth-of-type(1)").should("have.text", "Seats available!");
    cy.get('#content>p[class="promo_code"]').should(
      "have.text",
      promoCodeMessage
    );
  });

  it("05. There are seats found via Search with Invalid Promo Code", () => {
    const returning = "December (two years from now)";
    const promoCode = "AF3-FJK-419";
    const promoCodeMessage = `Sorry, code ${promoCode} is not valid`;

    // Perform Search
    cy.get("#departing").select(defaultDepart);
    cy.get("#returning").select(returning);
    cy.get("#promotional_code").type(promoCode);
    cy.get('[type="submit"]').click();

    // Assertion Search Results page
    cy.get("#content>p:nth-of-type(1)").should("have.text", "Seats available!");
    cy.get('#content>p[class="promo_code"]').should(
      "have.text",
      promoCodeMessage
    );
  });

  it("06. Go back to Landing page from Search Results page by Back link", () => {
    const returning = "December (two years from now)";

    // Perform Search
    cy.get("#departing").select(defaultDepart);
    cy.get("#returning").select(returning);
    cy.get('[type="submit"]').click();

    cy.get("#content>p:last-child>a").click();

    // Assertion Landing page
    cy.get("#content>h2").should("have.text", "Welcome to MarsAir!");
  });

  it("07. Go back to Landing page from Search Results page by the browser Back button", () => {
    const returning = "December (two years from now)";

    // Perform Search
    cy.get("#departing").select(defaultDepart);
    cy.get("#returning").select(returning);
    cy.get('[type="submit"]').click();

    cy.go(-1);

    // Assertion Landing page
    cy.get("#content>h2").should("have.text", "Welcome to MarsAir!");
  });
});
